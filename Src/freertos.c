/* USER CODE BEGIN Header */
/**

README
RTOS
TASK1 :循环发送串口到指纹模块 总共有三个步骤：1：录入指纹  2.获取指纹特征  3：对比指纹库  标志位分别为（task1 task2 task3）1表示该步骤完成
TASK2 ：OLED显示，task1位










*/

  
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "oled.h"
#include "finger.H"
#include "string.H"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
uint8_t  a[]="please press";
uint8_t  b[]="waiting..";
uint8_t  e[]="success!";
uint8_t  c[]="password:";
uint8_t  d[]="password error";
extern uint8_t data;
uint8_t  str_data[20];
uint8_t  password[6]={'8','6','9','3','5','2'};
uint8_t  str_data_p=0;;
uint8_t  state=0;//
uint8_t i=0;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);
void StartTask03(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityNormal, 0, 128);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, StartTask03, osPriorityNormal, 0, 128);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	/*
	TASK1 :循环发送串口到指纹模块 总共有三个步骤：1：录入指纹  2.获取指纹特征  3：对比指纹库  标志位分别为（task1 task2 task3）1表示该步骤完成
	
	*/
  /* Infinite loop */
  for(;;)
  {
				press= HAL_GPIO_ReadPin( GPIOC, GPIO_PIN_3);
			
			if(press==1){//当检测到手指按下，进入该if
				if(task1==1){/*如果task1完成则进入该if   否则进入else循环发送task1的请求，其中可能由于dma速度过快，所以发送完之后需要延时等待一下指纹模块应答，还有发送的字符串长度一定要严格按照长度来，过长
					则发送的数据后面会多几个0x00
					*/
					if(task2==1){//当task2任务完成，则进入该if  否则进入else循环发送task2的请求，和上一步一样

						if(task3!=1) {	//如果rask3没有完成，就循环发送请求，直到对方回应确认指纹
						 HAL_UART_Transmit_DMA(&huart2, (uint8_t *)DATA_send3_BUFFER,17);	
						HAL_Delay(500);
					HAL_UART_Receive_DMA(&huart2, (uint8_t *)DATA_receive_BUFFER,20);//串口2DMA接收初始化
					}
				}
					else{		HAL_UART_Transmit_DMA(&huart2, (uint8_t *)DATA_send2_BUFFER,13);
						HAL_Delay(500);
          HAL_UART_Receive_DMA(&huart2, (uint8_t *)DATA_receive_BUFFER,20);//串口2DMA接收初始化
					}

				}
				else{
				      HAL_UART_Transmit_DMA(&huart2, (uint8_t *)DATA_send1_BUFFER,12);	 
				      HAL_Delay(500);
					    HAL_UART_Receive_DMA(&huart2, (uint8_t *)DATA_receive_BUFFER,20);//串口2DMA接收初始化
	}
	}
    osDelay(1);
	}
  
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  { TIM1->CCR1=2000;
		TIM1->CCR2=2000;
		TIM1->CCR3=2000;
		TIM1->CCR4=2000;
		if(task3==1){
			TIM1->CCR1=0;
			TIM1->CCR2=0;
			TIM1->CCR3=0;
			TIM1->CCR4=0;
		OLED_ShowString(0,7,e,sizeof(e)); //如果指纹认证成功，显示成功
		if(press==0){ //当手指离开之后，延时一秒，然后所有标志位复位
			HAL_Delay(1000);task3=0;task2=0;task1=0;OLED_Clear();
		}
			
		}
		else{//如果没有认证成功或者正在验证，则进入else
		if(press==0){//没有按下的时候，task1和task2标志位复位
	   task2=0;task1=0;
		OLED_ShowString(0,0,a,sizeof(a)); 
		OLED_ShowString(0,1,c,sizeof(c)); 	
			if(state == 1)//按键的标志位，如果有按键按下，并且识别成功，则进入if显示出来，密码为6位
			OLED_ShowChar((i-1)*8,3,str_data[i-1],16);
			if(i>=6){//当密码超过6位则进入这个if
			OLED_Clear();i=0;state=0;  //全部复位，并且进行密码校验
			if( compare(str_data , password))//此函数会检测用户输入的6位密码和真是密码是否相同
			{task3=1;}//成功
			
			else {//oled提示失败
			OLED_ShowString(0,7,d,sizeof(d));  
			HAL_Delay(1000);
				OLED_Clear();
			}		
			}
			
				
				
		}

		else if(press==1){//如果指纹按下，则提示用户等待校验
		OLED_ShowString(0,7,b,sizeof(b)); 
		OLED_Clear();
			state=0;
			i=0;
		}
		
		
		
		
    osDelay(1);
  }}
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void const * argument)
{
  /* USER CODE BEGIN StartTask03 */
  /* Infinite loop */
  for(;;)
  {
	while(	!detect_dispaly()){//此函数会不断检测指纹有没有按下，如果按下了，就会返回对应的值，否则返回0，
	
	}
//只有成功检测到按下按键才会进入到这里
	HAL_Delay(500);	
	state = 1;//标志成功按下
	 str_data[i]=data;//将按下的键位赋值给数组
	i++;
    osDelay(1);
  }
  /* USER CODE END StartTask03 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
